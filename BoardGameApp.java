import java.util.Scanner;
public class BoardGameApp {
	public static void main(String[] args) {
		//initialize scanner
		Scanner reader = new Scanner(System.in);
		//welcome
		System.out.println("Welcome to my game!");
		//initialize variables
		Board gameboard = new Board();
		int numCastles = 5;
		int turns = 8;
		
		//while the castles 
		while(numCastles > 0 && turns <= 8) {
			System.out.println(gameboard);
			System.out.println("There are: " + numCastles + " Castles");
			System.out.println("You have: " + turns + " Turns");
			
			System.out.println("Enter First Row: ");
			int row = reader.nextInt();
			System.out.println("Enter First Column: ");
			int col = reader.nextInt();
			
			int token = gameboard.placeToken(row,col);
			
			if(token < 0) {
				System.out.println("Re-Enter a position, invalid");
				System.out.println("Enter First Row: ");
				row = reader.nextInt();
				System.out.println("Enter First Column: ");
				col = reader.nextInt();
			}
			if (token == 1) {
				System.out.println("There is a wall there!");
				turns--;
			}
			if (token == 0) {
				System.out.println("Succesful!");
				numCastles--;
				turns--;
			}
		}
		System.out.println(gameboard);
		if (numCastles == 0) {
			System.out.println("You Won!");
		} else {
			System.out.println("You Lost!");
		}
	}
}
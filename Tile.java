//Define enum Tile
public enum Tile {
	//Possible Values
    BLANK("_"),
	HIDDEN_WALL("_"),
    WALL("W"),
    CASTLE("C");
	//Initialize The Name Variable Which Represents The Character
    private final String name;

    private Tile(String name) {
        this.name = name;
    }
	//Name Getter
    public String getName() {
        return name;
    }
}

import java.util.Random;
public class Board {
	//2D Array For A Square
    private Tile[][] grid;
	final int size = 5;
    public Board() {
        //Initialize The Grid To A 5x5, dont use magic numbers
        grid = new Tile[size][size];
		Random rng = new Random();	
        for (int i = 0; i < grid.length; i++) {
			//set a random one per row to be a hidden wall
			int randomspot = rng.nextInt(grid.length);
            for (int j = 0; j < grid.length; j++) {
                grid[i][j] = Tile.BLANK;
				grid[i][randomspot] = Tile.HIDDEN_WALL;
            }
        }
    }
    public String toString() {
        String result = "";
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                result += grid[i][j].getName() + " ";
            }
            result += "\n";
        }
        return result;
    }

    public int placeToken(int row, int col) {
        //Check If Row Is Valid
        if (row < 0 || row >= size || col < 0 || col >= size) {
            return -2;
        }

        //Check If Position is Not Already Taken, Then Return Value
        if (grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
            return -1;
        } else if(grid[row][col] == Tile.HIDDEN_WALL) {
			grid[row][col] = Tile.WALL;
			return 1;
		}else {
			grid[row][col] = Tile.CASTLE;
            return 0;
        }
    }
}
